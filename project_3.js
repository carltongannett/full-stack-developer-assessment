document.addEventListener('DOMContentLoaded', function() {  
var log = [];

/*
Explain what a callback function is and provide a simple code example of one. The explanation should be included in the file as a comment. */
function callback_example(a, callback) {
/*
A callback is a function that is passed as an argument to another function or method.
this function takes the parameters "a" and  "callback" and returns the result of the "callback" run with "a" passed to it. https://developer.mozilla.org/en-US/docs/Glossary/Callback_function
*/

return callback(a);
}

callback_example("This message passed along with alert() to callback_example()", alert);

/*
Give two arrays, a "Working" array and a "Deleted" array, write functions to add and subtract items from the "working" array while also keeping track of the "deleted" */
function array_tracker(arr) {
const tracker = {
  "working": [...arr],
  "deleted": [],
  "delete": function (start, end) {
    //wraps array.prototype.splice and pushes the return values to deleted
    this.deleted.push(...this.working.splice(start, end));
    return this;
  }
};
return tracker;
}
var arr = new array_tracker(Object.keys(Array(20).fill(1)));
arr.delete(0, 5);
log.push("array_tracker: ",arr, "working: ", arr.working, "deleted: ", arr.deleted);
/* result is  
{working: Array(15), deleted: Array(5), delete: ƒ}
delete: ƒ (start, end)
deleted: (5) ["0", "1", "2", "3", "4"]
working: (15) ["5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"]
__proto__: Object
 */


// naive anagram detector, doesn't ignore whitespace but does ignore case;
function is_anagram(a, b) {
  return Array.from(a.toUpperCase()).reverse().join("") === b.toUpperCase();
}
log.push(
"is_anagram :"
[is_anagram("abc", "CBA"), // => true
is_anagram("ab c", "CBA"), // => false
is_anagram("abc", "CB A")] // => false
);

//better anagram detector that uses regex and recursion
function is_anagramRegex(a, b) {
  let rev = function rev(str) {return (str === '') ? '' : rev(str.substr(1)) + str.charAt(0)};
  let reg = /\s+/g;
  return rev(a.toUpperCase().replaceAll(reg, c => "")) === b.toUpperCase().replaceAll(reg, c => "");
}
log.push( "is_anagramRegex: ",
is_anagramRegex("Carlton", "n O t L r A C"), // => true
is_anagramRegex("Carlton", "n O t L r A C 7") // => false
);

log.forEach(i=>document.write("<br>"+JSON.stringify(i)));

}, false); 

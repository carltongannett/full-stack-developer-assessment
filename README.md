# Full Stack Developer - Assessment
Start by pulling down a copy of the repo we've shared with you. We will be requiring you to keep track of your changes in Git and publicly publishing your repo when you're ready. As far as publishing, you may use any git hosting service you would like, GitHub, BitBucket, GitLab, etc.

Please create a new file for each project below (name examples: project_01.php, project_one.js, proj_1.py, etc.).

## Project 01 - FizzBuzz

Wrote this in PHP and CSS and have a live version available here: https://repl.carltongannett.com/project_1.php


## Project 02 - PHP Mini Project
Live version that stores serialized object data in $_SESSION variables and validates input. Uses PHP / Bootstrap
https://repl.carltongannett.com/

## Project 03 - JavaScript Mini Project
live version output sent to document.write();
https://repl.carltongannett.com/project_3.php


## That's It!
When you're done, please publish your repo either on GitHub, GitLab, BitBucket or another publicly available git repo hosting service of your choosing and email it to both (CC'ing one of us is fine, no need for seperate emails) michael@kinetic.com and steven@kinetic.com. If you could also tell us what you thought overall of this assesment in your email, that would also be helpful. We're constantly trying to improve our processes and feedback is always welcome!

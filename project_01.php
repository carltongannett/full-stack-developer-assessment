<!DOCTYPE html>
<html>

<body>

<?php
//PHP 7.2.24-0
function fizzBuzz(int $fbnum1 = 3, int $fbnum2 = 5, int $fbstart = 1, int $fbstop = 100) {


  $fbnum3 = $fbnum1*$fbnum2; 

//STYLE Section
  $style = "
  <style>div:nth-child($fbnum1"."n),div:nth-child($fbnum2"."n)  {
  color: white;
  font-weight:800;
}
div:nth-child($fbnum1"."n) {
  background:red;
}
div:nth-child($fbnum2"."n) {
  background:blue;
}
div:nth-child($fbnum3"."n) {
  background:purple;
}
</style>";

//FIZZBUZZ section
for ($x = $fbstart; $x <= $fbstop; $x++) {

  if ($x % $fbnum3 === 0) 
  {
    echo "<div>FizzBuzz</div>";
  }
  elseif($x % $fbnum1 === 0) {
    echo "<div>Fizz</div>";
  }
  elseif($x % $fbnum2 === 0) 
  {
    echo "<div>Buzz</div>";
  }
  else 
  {
    echo "<div>$x</div>";
  }
}
echo $style;
}

fizzBuzz();


?>

</body>
</html>

<?php 

class Cart
{
    /**
     * Example of cart_items array structure
     * $cart_items = ['product_id' => ['quantity' => quantity, 'unit_price' => unit_price]]
     */
    // private cart_items = array();
    public $tax_percent = 10;
    private $price_list = [
        // 'product_id' => price,
        // these prices are in pennies
        '1001' => 1299,
        '1002' => 927,
        '1003' => 1000,
        '1004' => 419,
        '1005' => 675,
    ];  
 /*
 - handle adding items, removing items, updating item quantities and clearing all items from the cart, then return the updated cart. 
- calculate and return the total of all the items in the cart with tax applied
 */
    

  function __construct($items)
  {

        $this->cart_items = $items;
        $this->validate_items();
        $this->calculate_total();
  }
  function get_available() {
    return array_keys($this->price_list);
  }
  private function isin_cart(string $id)
  {
  //return if item exists in cart_items array
      return array_key_exists($id,$this->cart_items);
  }
  function validate_items() {
        foreach (($this->cart_items) as $key => $value)
        {
          
          if ((!isset($this->price_list[$key]))||($value["quantity"] <= 0))
          {
            unset($this->cart_items[$key]);
          }
          
        }
  }
  

  private function get_price(string $id) 
  {
      if (isset($this->price_list[$id]))
      {return $this->price_list[$id];}
      else {
        return null;
      }
  }

  function add_item($id)
  {// if item is in cart increase by quantity, else add item to cart returns cart with item validated.
    if ($this->isin_cart($id))
      {
        $this->cart_items[$id]["quantity"]++;
      }
    else
      {
        $this->cart_items[$id] = ["quantity"=>1, "unit_price"=>$this->get_price($id)];
      }
    
  }

  function remove_item(string $id)
  {// if item is in cart decrement and returns cart with item validated
    if ($this->isin_cart($id))
      {
        $this->cart_items[$id]["quantity"]--;
      }
    
  }

  function set_quantity(string $id, int $quant)
  { //set item quantity and return cart with validated item
    if ($this->isin_cart($id))
    {
      $this->cart_items[$id]["quantity"] = $quant;
    }
    else {
      $this->cart_items[$id] = ["quantity"=>$quant, "unit_price"=>$this->get_price($id)];
    }
    
  }

  function clear_cart() 
  {
        $this->cart_items = [];
  }

  private function calculate_total() {
      
      $tax = (100+$this->tax_percent)/100; 
      $pretax = array_reduce($this->cart_items, function ($sum, $item) {
        $cost = $item["quantity"]*$item["unit_price"];
        return $sum + $cost;
      }, 0);

      $this->total = round($pretax*$tax, 0, PHP_ROUND_HALF_UP);      //rounding half up for sales tax
return $this;
    }



}

?>

